
package.path = '.;../third-party/Penlight/lua/?.lua;../third-party/json.lua/?.lua'
local json = require 'json'
local pretty = require 'pl.pretty' 

local output_luatable = function(lua_table,path)
    --printTable(lua_table,path)
    local content = pretty.write(lua_table)
    local file = io.open(path,'w+')
    file:write("return\n")
    file:write(content)
    file:close()
end 

local main = function()
    local lfs = require 'lfs'
    local dir_name = './Configs'
    for file in lfs.dir(dir_name) do
        if string.match(file,'.json$') then
           
            local f,err = io.open(dir_name..'/'..file, "r")
            local content = f:read("*all")
            f:close()
            local lua_obj = json.decode(content)
            local index = string.find(file,'.json')
            local name = string.sub(file,0,index-1)
            local target_path = './LuaTables/'..name..'.lua'
            if lua_obj == nil then
                print(file)
            end
            output_luatable(lua_obj,target_path)
        end
    end
end

main()