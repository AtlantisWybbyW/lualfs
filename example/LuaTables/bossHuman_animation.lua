return
{
  groupList = {
    {
      actionList = {
        {
          animationName = "boss_human_idle",
          endFrame = 100,
          key = "idle",
          loopType = 2,
          startFrame = 0
        }
      },
      default = true,
      desc = "站立",
      name = "stand"
    },
    {
      actionList = {
        {
          animationName = "boss_human_die",
          endFrame = 100,
          key = "dead",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "死亡",
      name = "dead"
    },
    {
      actionList = {
        {
          animationName = "boss_human_stand",
          endFrame = 100,
          key = "fall",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "下落",
      name = "fall"
    },
    {
      actionList = {
        {
          animationName = "boss_human_stand",
          endFrame = 100,
          key = "landing",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "落地",
      name = "landing"
    },
    {
      actionList = {
        {
          animationName = "boss_human_run",
          endFrame = 60,
          key = "walk",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "走路",
      name = "walk"
    },
    {
      actionList = {
        {
          animationName = "boss_human_run",
          endFrame = 60,
          key = "run",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "跑动",
      name = "run"
    },
    {
      actionList = {
        {
          animationName = "boss_human_jump",
          endFrame = 6,
          key = "jump",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "跳跃",
      name = "jump"
    },
    {
      actionList = {
        {
          animationName = "boss_human_die",
          endFrame = 70,
          key = "die",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "死亡",
      name = "die"
    },
    {
      actionList = {
        {
          animationName = "nearAttack1",
          endFrame = 53,
          key = "nearAttack1",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "近战攻击一次",
      name = "nearAttack1"
    },
    {
      actionList = {
        {
          animationName = "boss_human_skill01",
          endFrame = 62,
          key = "nearAttack2",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "近战攻击两次",
      name = "nearAttack2"
    },
    {
      actionList = {
        {
          animationName = "boss_human_jump",
          endFrame = 6,
          key = "jump",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "跳跃冲刺攻击",
      name = "jumpSprintAttack_1"
    },
    {
      actionList = {
        {
          animationName = "boss_human_skill02_02",
          endFrame = 36,
          key = "sprint",
          loopType = 1,
          startFrame = 11
        },
        {
          animationName = "boss_human_idle",
          endFrame = 30,
          key = "stand",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "跳跃冲刺攻击",
      name = "jumpSprintAttack_2"
    },
    {
      actionList = {
        {
          animationName = "boss_human_skill03",
          endFrame = 45,
          key = "farAttack1",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "远程攻击一次",
      name = "farAttack1"
    },
    {
      actionList = {
        {
          animationName = "boss_human_skill04",
          endFrame = 56,
          key = "farAttack3",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "远程攻击三次",
      name = "farAttack3"
    },
    {
      actionList = {
        {
          animationName = "boss_human_skill05",
          endFrame = 25,
          key = "hideDown",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "往下隐藏，变身血池",
      name = "hideDown"
    },
    {
      actionList = {
        {
          animationName = "boss_human_skill05_02",
          endFrame = 21,
          key = "showUp",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "从地面显示出来",
      name = "showUp"
    },
    {
      actionList = {
        {
          animationName = "boss_human_move",
          endFrame = 80,
          key = "move",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "瞬移",
      name = "move"
    },
    {
      actionList = {
        {
          animationName = "boss_human_fall",
          endFrame = 110,
          key = "fallattack",
          loopType = 1,
          startFrame = 0,
          toDuration = 0
        }
      },
      desc = "下落攻击",
      name = "fall_attack"
    },
    {
      actionList = {
        {
          animationName = "boss_human_fall",
          endFrame = 110,
          key = "fallattack",
          loopType = 1,
          startFrame = 20,
          toDuration = 0
        }
      },
      desc = "下落攻击",
      name = "fall_attack_fast"
    },
    {
      actionList = {
        {
          animationName = "nearAttack1",
          endFrame = 53,
          key = "nearAttack1",
          loopType = 1,
          startFrame = 0
        },
        {
          animationName = "boss_human_move",
          endFrame = 80,
          key = "move",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "下落攻击",
      name = "nearAttack1_move"
    }
  },
  name = "bossHuman_animation"
}