return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "向右",
          fixedTime = 1.7,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            8.5,
            0,
            0
          }
        }
      },
      desc = "吸怪攻击盒",
      isDefault = true,
      isLoop = true,
      name = "run"
    }
  },
  name = "bullet_move_3004"
}