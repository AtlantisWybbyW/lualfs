return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "水平飞行",
          fixedTime = -1,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            -9,
            0,
            0
          }
        }
      },
      desc = "默认水平飞行",
      isDefault = true,
      isLoop = true,
      name = "run"
    }
  },
  name = "bulletHori_move_2014"
}