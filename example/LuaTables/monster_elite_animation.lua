return
{
  groupList = {
    {
      actionList = {
        {
          animationName = "idle",
          endFrame = 72,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      default = true,
      desc = "站立",
      name = "stand"
    },
    {
      actionList = {
        {
          animationName = "battleidle",
          endFrame = 72,
          key = "battleidle",
          loopType = 2,
          startFrame = 0
        }
      },
      default = true,
      desc = "站立",
      name = "battleidle"
    },
    {
      actionList = {
        {
          animationName = "die",
          endFrame = 72,
          key = "dead",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "死亡",
      name = "dead"
    },
    {
      actionList = {
        {
          animationName = "skill01",
          endFrame = 72,
          key = "skill01",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "攻击",
      name = "skill01"
    },
    {
      actionList = {
        {
          animationName = "skill02",
          endFrame = 85,
          key = "skill02",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "落石",
      name = "skill02"
    }
  },
  name = "monster_elite_animation"
}