return
{
  groupList = {
    {
      actionList = {
        {
          animationName = "boss_tree_idle",
          endFrame = 90,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      default = true,
      desc = "站立",
      name = "stand"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_idle",
          endFrame = 90,
          key = "dead",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "死亡",
      name = "dead"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_idle",
          endFrame = 90,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "走步",
      name = "walk"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_idle",
          endFrame = 90,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "跑步",
      name = "run"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_skill01",
          endFrame = 90,
          key = "skill01",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "技能1",
      name = "skill01"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_skill02",
          endFrame = 120,
          key = "skill02",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "技能2",
      name = "skill02"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_skill03",
          endFrame = 117,
          key = "skill03",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "技能3",
      name = "skill03"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_skill04_01",
          endFrame = 104,
          key = "attackLeft",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "左胳膊攻击",
      name = "attackLeft"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_skill04_03",
          endFrame = 104,
          key = "attackLeft_2",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "左胳膊旋转攻击",
      name = "attackLeft_2"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_skill04_02",
          endFrame = 104,
          key = "attackRight",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "右胳膊攻击",
      name = "attackRight"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_skill04_04",
          endFrame = 104,
          key = "attackRight_2",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "右胳膊旋转攻击",
      name = "attackRight_2"
    },
    {
      actionList = {
        {
          animationName = "boss_tree_die",
          endFrame = 78,
          key = "die",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "死亡",
      name = "die"
    }
  },
  name = "bossTree_animation"
}