return
{
  name = "trigger_skill",
  skillList = {
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "相机切换触发器",
      skill_id = 1001,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SwitchCamera",
                cameraCfg = {
                  cameraState = 1,
                  isFollow = true,
                  transitionTime = 1
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "相机切换触发器",
      skill_id = 1002,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SwitchCamera",
                cameraCfg = {
                  cameraState = 4,
                  isFollow = true,
                  transitionTime = 1
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "相机切换触发器",
      skill_id = 1003,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SwitchCamera",
                cameraCfg = {
                  cameraState = 5,
                  isFollow = true,
                  transitionTime = 1
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "相机切换触发器",
      skill_id = 1004,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SwitchCamera",
                cameraCfg = {
                  cameraState = 3,
                  isFollow = true,
                  transitionTime = 1
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "相机切换触发器",
      skill_id = 1005,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SwitchCamera",
                cameraCfg = {
                  cameraState = 1,
                  isFollow = true,
                  transitionTime = 1
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "第一幕传送门",
      skill_id = 2001,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "LevelPortalTrigger",
                levelCfg = {
                  levelName = "level2"
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "第二幕传送门",
      skill_id = 2002,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "LevelPortalTrigger",
                levelCfg = {
                  levelName = "level3"
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "移动平台",
      skill_id = 3001,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SendEventTrigger",
                event = {
                  selfEventName = "onStandToPlatform",
                  selfEventParam = {
                  },
                  targetEventName = "",
                  targetEventParam = {
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "移动平台",
      skill_id = 3002,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SendEventTrigger",
                event = {
                  selfEventName = "onExitStandToPlatform",
                  selfEventParam = {
                  },
                  targetEventName = "",
                  targetEventParam = {
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "伤害触发器技能",
      skill_id = 3003,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SendEventTrigger",
                event = {
                  selfEventName = "",
                  selfEventParam = {
                  },
                  targetEventName = "onDamage",
                  targetEventParam = {
                    99999
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_id = 3004,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SendEventTrigger",
                event = {
                  selfEventName = "",
                  selfEventParam = {
                  },
                  targetEventName = "onEnterRebirthPoint",
                  targetEventParam = {
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "短间隔伤害触发器技能",
      skill_id = 3005,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 1,
          tag_config = {
            {
              action = {
                action_type = "SendEventTrigger",
                event = {
                  selfEventName = "",
                  selfEventParam = {
                  },
                  targetEventName = "onDamage",
                  targetEventParam = {
                    99999
                  }
                }
              },
              trigger_second = 0.25
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "被动技-给目标加伤害buff",
      skill_id = 4001,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "OnTriggerBuff",
                config = {
                  isAdd = true,
                  selfBuffId = 0,
                  targetBuffId = 10012
                }
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              trigger_second = 0.01
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "被动技-给目标移除伤害buff",
      skill_id = 4002,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "OnTriggerBuff",
                config = {
                  isAdd = false,
                  selfBuffId = 0,
                  targetBuffId = 10012
                }
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              trigger_second = 0.01
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "地刺-给目标加伤害buff",
      skill_id = 4003,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "OnTriggerBuff",
                config = {
                  isAdd = true,
                  selfBuffId = 0,
                  targetBuffId = 10014
                }
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              trigger_second = 0.01
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "地刺-给目标移除伤害buff",
      skill_id = 4004,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "OnTriggerBuff",
                config = {
                  isAdd = false,
                  selfBuffId = 0,
                  targetBuffId = 10014
                }
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              trigger_second = 0.01
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 10,
      skill_desc = "滑索-技能测试场景",
      skill_id = 4005,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "SendEventTrigger",
                event = {
                  selfEventName = "",
                  selfEventParam = {
                  },
                  targetEventName = "onEnterSlidingRope",
                  targetEventParam = {
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "洪水-给目标加伤害buff",
      skill_id = 4006,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "OnTriggerBuff",
                config = {
                  isAdd = true,
                  selfBuffId = 0,
                  targetBuffId = 10017
                }
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              trigger_second = 0.01
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "洪水-给目标移除伤害buff",
      skill_id = 4007,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "OnTriggerBuff",
                config = {
                  isAdd = false,
                  selfBuffId = 0,
                  targetBuffId = 10017
                }
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              trigger_second = 0.01
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "进入场景交互",
      skill_id = 5001,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "PosEventToUI",
                eventName = "enterInteraction",
                eventParam = {
                  interactionId = 100001,
                  skillId = 6001
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "退出场景交互",
      skill_id = 5002,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "PosEventToUI",
                eventName = "exitInteraction"
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "进入场景交互",
      skill_id = 5003,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "PosEventToUI",
                eventName = "enterInteraction",
                eventParam = {
                  interactionId = 100002
                }
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "ShowHeadTopTip",
                config = {
                  interactionId = 100002,
                  isShow = true,
                  offPos = {
                    0,
                    1.5,
                    0
                  },
                  type = "self"
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "退出场景交互",
      skill_id = 5004,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "PosEventToUI",
                eventName = "exitInteraction"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "ShowHeadTopTip",
                config = {
                  isShow = false,
                  type = "self"
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_desc = "尸体消失，播放特效",
      skill_id = 6001,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 3,
          tag_config = {
            {
              action = {
                action_type = "TriggerTargetSkillState",
                skillId = 1012
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "ShowHeadTopTip",
                config = {
                  interactionId = 100001,
                  isShow = true,
                  offPos = {
                    0,
                    1.5,
                    0
                  },
                  type = "target"
                }
              },
              trigger_second = 0.5
            },
            {
              action = {
                action_type = "ShowHeadTopTip",
                config = {
                  isShow = false,
                  type = "target"
                }
              },
              trigger_second = 1
            },
            {
              action = {
                action_type = "CreateBullet",
                actorViewPrefabId = 1007,
                delaySecond = 0,
                layerMaskName = {
                  "MainCharacter"
                },
                moveConfigName = "effectToPlayer_move",
                skillList = {
                  2108
                },
                startPosOffset = {
                  0,
                  0,
                  0
                }
              },
              trigger_second = 1.1
            },
            {
              action = {
                action_type = "ShowUIPanel",
                config = {
                  guideSkillId = 1001,
                  panelName = "NewSkillGetWindowsPanel"
                }
              },
              trigger_second = 2.5
            },
            {
              action = {
                action_type = "OnTriggerBuff",
                config = {
                  isAdd = true,
                  selfBuffId = 0,
                  targetBuffId = 10019
                }
              },
              trigger_second = 2.6
            },
            {
              action = {
                action_type = "DestroySelf"
              },
              trigger_second = 2.9
            }
          },
          timeline_id = 1
        }
      }
    }
  }
}