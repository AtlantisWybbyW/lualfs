return
{
  groupList = {
    {
      actionList = {
        {
          animationName = "idle",
          endFrame = 72,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      default = true,
      desc = "站立",
      name = "stand"
    },
    {
      actionList = {
        {
          animationName = "die",
          endFrame = 100,
          key = "dead",
          loopType = 1,
          startFrame = 10
        }
      },
      desc = "死亡",
      name = "dead"
    }
  },
  name = "destroyPlatform_animation"
}