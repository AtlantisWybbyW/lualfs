return
{
  groupList = {
    {
      actions = {
        {
          accelerationY = -30,
          actionType = "constantHWithGravity",
          ignoreY = true,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = -1,
          useInput = true,
          velocity = {
            6,
            0,
            0
          }
        }
      },
      desc = "基础移动：站立，跑",
      isLoop = true,
      name = "run"
    },
    {
      actions = {
        {
          accelerationY = -30,
          actionType = "constantHWithGravity",
          ignoreY = true,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = -1,
          useInput = false,
          velocity = {
            0,
            0,
            0
          }
        }
      },
      desc = "死亡，无移动，有重力",
      isLoop = true,
      name = "die"
    },
    {
      actions = {
        {
          accelerationY = -30,
          actionType = "constantHWithGravity",
          ignoreY = true,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = 3.3,
          useInput = true,
          velocity = {
            4,
            0,
            0
          }
        },
        {
          accelerationY = -30,
          actionType = "constantHWithGravity",
          ignoreY = true,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = 1.4,
          useInput = true,
          velocity = {
            0,
            0,
            0
          }
        }
      },
      desc = "大招移动",
      isLoop = true,
      name = "skill02_run"
    },
    {
      actions = {
        {
          accelerationY = 0,
          actionType = "constantHWithGravity",
          flipXChangeSpeedX = 3,
          ignoreY = true,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = 0.1,
          useInput = true,
          velocity = {
            6,
            0,
            0
          }
        },
        {
          accelerationY = -50,
          actionType = "constantHWithGravity",
          flipXChangeSpeedX = 3,
          ignoreY = true,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = -1,
          useInput = true,
          velocity = {
            6,
            0,
            0
          }
        }
      },
      desc = "下落",
      isLoop = true,
      name = "fall"
    },
    {
      actions = {
        {
          accelerationY = -45,
          actionType = "constantHWithGravity",
          flipXChangeSpeedX = 3,
          ignoreY = true,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = -1,
          useInput = true,
          velocity = {
            6,
            0,
            0
          }
        }
      },
      desc = "空中上升",
      isLoop = true,
      name = "airUp"
    },
    {
      actions = {
        {
          accelerationY = -45,
          actionType = "constantHWithGravity",
          correctVelocityHType = "default",
          flipXChangeSpeedX = 2,
          ignoreY = false,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = 0.06,
          targetPosTriggerType = "default",
          useInput = true,
          velocity = {
            6,
            13.5,
            0
          }
        }
      },
      desc = "上跳",
      isLoop = false,
      name = "jump"
    },
    {
      actions = {
        {
          accelerationY = -45,
          actionType = "constantHWithGravity",
          correctVelocityHType = "default",
          flipXChangeSpeedX = 2,
          ignoreY = false,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = 0.06,
          targetPosTriggerType = "default",
          useInput = true,
          velocity = {
            6,
            11.5,
            0
          }
        }
      },
      desc = "二段跳",
      isLoop = false,
      name = "jump_2"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地等0.6s",
          fixedTime = 0.6,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "negative",
          desc = "朝向反方向匀速移动0.2s",
          fixedTime = 0.2,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            10,
            0,
            0
          }
        }
      },
      desc = "普攻后移",
      isLoop = false,
      name = "role_skill01"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地0.1s",
          fixedTime = 0.1,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "前进0.3秒",
          fixedTime = 0.3,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            4,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "第一段原地0.3s",
          fixedTime = 0.4,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "前进0.2秒",
          fixedTime = 0.2,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            6,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "第二段原地0.3s",
          fixedTime = 0.4,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        }
      },
      desc = "不停向前攻击并位移",
      isLoop = false,
      name = "role_skill02"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "前进0.3秒",
          fixedTime = 0.3,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            7,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "第三段原地0.3s",
          fixedTime = 0.4,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "前进0.2秒",
          fixedTime = 0.2,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            6,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "第四段原地0.3s",
          fixedTime = 0.35,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "前进0.2秒",
          fixedTime = 0.15,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            6,
            0,
            0
          }
        }
      },
      desc = "不停向前攻击并位移",
      isLoop = false,
      name = "role_skill02_2"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 0.25,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            13,
            -0.1,
            0
          }
        }
      },
      desc = "水平冲刺",
      isLoop = false,
      name = "sprint"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地0.3s",
          fixedTime = 0,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = -1,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            -40,
            0
          }
        }
      },
      desc = "下落",
      isLoop = true,
      name = "fast_fall"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "speedX",
          desc = "向当前朝向匀速位移一段时间",
          fixedSpeed = 20,
          fixedTime = 2.5,
          fixedType = "speed",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              0,
              4,
              0
            }
          },
          targetPosTriggerType = "groupStart"
        }
      },
      desc = "匀速冲向一个世界坐标点",
      isLoop = false,
      name = "sprintWorldPos1"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地等待1s",
          fixedTime = 1,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "teleport",
          changeFaceType = "ignore",
          desc = "瞬移到世界坐标点",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              0,
              4,
              0
            }
          },
          targetPosTriggerType = "groupStart",
          targetPosition = {
            10,
            6,
            0
          }
        }
      },
      desc = "等待1s闪现到一个世界坐标点",
      isLoop = false,
      name = "teleport1"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "向落地点匀速位移一段时间",
          fixedSpeed = 12,
          fixedType = "speed",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              -6,
              2,
              -2.8
            }
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            13,
            -0.1,
            0
          }
        }
      },
      desc = "技能测试场景滑索",
      isLoop = false,
      name = "slidingRope1"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          fixedTime = 2.5,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              30,
              4,
              0
            }
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            10,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          fixedTime = 2.5,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "none",
            worldPosition = {
              30,
              4,
              0
            }
          },
          targetPosTriggerType = "default",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "teleport",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              30,
              4,
              0
            }
          },
          targetPosTriggerType = "groupStart",
          targetPosition = {
            10,
            6,
            0
          }
        }
      },
      desc = "测试保留",
      isLoop = true,
      name = "test"
    }
  },
  name = "player_move"
}