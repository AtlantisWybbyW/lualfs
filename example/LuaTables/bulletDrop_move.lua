return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "往下落",
          fixedTime = -1,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            -6,
            0
          }
        }
      },
      desc = "往下落",
      isDefault = true,
      isLoop = true,
      name = "run"
    }
  },
  name = "bulletDrop_move"
}