return
{
  groupList = {
    {
      actionList = {
        {
          animationName = "idle",
          endFrame = 52,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      default = true,
      desc = "站立",
      name = "stand"
    },
    {
      actionList = {
        {
          animationName = "die",
          endFrame = 60,
          key = "dead",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "死亡",
      name = "dead"
    },
    {
      actionList = {
        {
          animationName = "run",
          endFrame = 30,
          key = "run",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "跑步",
      name = "run"
    },
    {
      actionList = {
        {
          animationName = "attack",
          endFrame = 85,
          key = "attack",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "普攻",
      name = "attack"
    }
  },
  name = "monster02_animation"
}