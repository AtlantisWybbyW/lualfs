return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "朝玩家飞行",
          fixedSpeed = 8,
          fixedTime = -1,
          fixedType = "timeAndSpeed",
          moveTargetConfig = {
            key = "run",
            targetType = "worldPosDynamic"
          },
          targetPosTriggerType = "actionStart"
        }
      },
      desc = "朝传入的外部位置移动",
      isDefault = true,
      isLoop = true,
      name = "run"
    }
  },
  name = "bulletToDynamicPosition_move"
}