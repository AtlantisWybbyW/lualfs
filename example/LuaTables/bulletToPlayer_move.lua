return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "朝玩家飞行",
          fixedSpeed = 8,
          fixedTime = -1,
          fixedType = "timeAndSpeed",
          moveTargetConfig = {
            bodyPart = "pelvic",
            targetType = "targetActor"
          },
          targetPosTriggerType = "actionStart"
        }
      },
      desc = "朝玩家飞行",
      isDefault = true,
      isLoop = true,
      name = "run"
    }
  },
  name = "bulletToPlayer_move"
}