return
{
  name = "player_skill",
  skillList = {
    {
      condition_config = {
        {
          buffId = 10011,
          compareType = "less",
          condition_type = "BuffLayer",
          layerNum = 2
        }
      },
      skill_cd = 0,
      skill_desc = "跳跃",
      skill_id = 1001,
      timeline_config = {
        {
          duration_second = 0.5,
          tag_config = {
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 2
              },
              condition = {
                condition_type = "IsInGraceTimeOrGround",
                isInTime = true
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 3
              },
              condition = {
                conditionList = {
                  {
                    condition_type = "IsInGraceTimeOrGround",
                    isInTime = false
                  },
                  {
                    buffId = 10011,
                    compareType = "less",
                    condition_type = "BuffLayer",
                    layerNum = 2
                  }
                },
                condition_type = "And"
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        },
        {
          duration_second = 0.5,
          tag_config = {
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "jump",
                moveGroupName = "jump"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 1001011,
                scale = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "moveGroupFinish",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddBuff",
                buffId = 10011,
                buffLayer = 1
              },
              trigger_second = 0
            }
          },
          timeline_id = 2
        },
        {
          duration_second = 0.5,
          tag_config = {
            {
              action = {
                action_type = "ChangeBodyStateAnimation",
                animationKey = "fall_2",
                bodyState = 3
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "PlayerAttackState",
                moveGroupName = "jump_2"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "moveGroupFinish",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    -0.2,
                    0.4,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 1001006,
                scale = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddBuff",
                buffId = 10011,
                buffLayer = 2
              },
              trigger_second = 0
            }
          },
          timeline_id = 3
        },
        {
          duration_second = 0.1,
          tag_config = {
            {
              action = {
                action_type = "OwnerReleaseSkill",
                isInterruptCurSkill = true,
                skillId = 1003
              },
              trigger_second = 0
            }
          },
          timeline_id = 4
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0.6,
      skill_desc = "普攻",
      skill_id = 1002,
      timeline_config = {
        {
          duration_second = 0.5,
          tag_config = {
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "animationUpperGroupFinish",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "attack"
              },
              condition = {
                condition_type = "IsOnGround",
                on_ground = true
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "PlayUpperAnimation",
                animationUpperGroupName = "upperAttack",
                isEndFinish = true
              },
              condition = {
                condition_type = "IsOnGround",
                on_ground = false
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddEffect",
                addActorConfig = {
                  actorType = "self"
                },
                addParentType = "actor",
                addPositionConfig = {
                  getType = "getBaseOnWorld",
                  worldAbsolutePosition = {
                    0,
                    0,
                    0
                  }
                },
                addRotationConfig = {
                  getType = "getBaseOnWorld",
                  worldAbsoluteRotation = {
                    0,
                    0,
                    0
                  }
                },
                effectId = 1001005,
                scale = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "CreateBullet",
                actorViewPrefabId = 1001,
                delaySecond = 0,
                hitMaxNum = 1,
                layerMaskName = {
                  "Monster"
                },
                moveConfigName = "bulletHori_move",
                skillList = {
                  2103
                },
                startPosOffset = {
                  0.6,
                  0.9,
                  0
                }
              },
              trigger_second = 0.1
            },
            {
              action = {
                action_type = "CreateBullet",
                actorViewPrefabId = 1001,
                delaySecond = 0.1,
                hitMaxNum = 1,
                layerMaskName = {
                  "Monster"
                },
                moveConfigName = "bulletHori_move",
                skillList = {
                  2103
                },
                startPosOffset = {
                  0.6,
                  0.9,
                  0
                }
              },
              trigger_second = 0.1
            },
            {
              action = {
                action_type = "CreateBullet",
                actorViewPrefabId = 1001,
                delaySecond = 0.2,
                hitMaxNum = 1,
                layerMaskName = {
                  "Monster"
                },
                moveConfigName = "bulletHori_move",
                skillList = {
                  2103
                },
                startPosOffset = {
                  0.6,
                  0.9,
                  0
                }
              },
              trigger_second = 0.1
            },
            {
              action = {
                action_type = "CanInterruptSkill"
              },
              trigger_second = 0.3
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0.1,
      skill_desc = "滑铲",
      skill_id = 1003,
      timeline_config = {
        {
          duration_second = 0.25,
          tag_config = {
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 3
              },
              condition = {
                condition_type = "IsOnGround",
                on_ground = false
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 2
              },
              condition = {
                condition_type = "IsOnGround",
                on_ground = true
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        },
        {
          duration_second = 0.25,
          tag_config = {
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 3
              },
              condition = {
                condition_type = "IsOnGround",
                on_ground = false
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "CanInterruptSkill"
              },
              trigger_second = 0.1
            },
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "crossStart",
                moveGroupName = "sprint"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddEffect",
                addActorConfig = {
                  actorType = "self"
                },
                addParentType = "actor",
                addPositionConfig = {
                  getType = "getBaseOnWorld",
                  worldAbsolutePosition = {
                    0,
                    0,
                    0
                  }
                },
                addRotationConfig = {
                  getType = "getBaseOnWorld",
                  worldAbsoluteRotation = {
                    0,
                    0,
                    0
                  }
                },
                effectId = 1015,
                onEndStop = true,
                scale = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 1,
                  isActivate = false
                },
                isEndEnable = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 2,
                  isActivate = true
                },
                isEndEnable = 2
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "ChangeHeight",
                config = {
                  height = 0.7,
                  normalHeight = 1.4
                }
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "PlaySound",
                soundEvent = "skill_zj_rush"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "CanInterruptSkill"
              },
              trigger_second = 0.1
            }
          },
          timeline_id = 2
        },
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "fall_2",
                moveGroupName = "fast_fall"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 1001010,
                scale = 1
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "ShakeCam",
                config = {
                  impulseId = 104
                }
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            }
          },
          timeline_id = 3
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 0,
      skill_id = 1004,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "moveGroupFinish",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "crossStart",
                moveGroupName = "sprintWorldPos1"
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      skill_cd = 3,
      skill_desc = "传送",
      skill_id = 1005,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "StopCurTimeline",
                stopType = 1
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "moveGroupFinish",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "crossStart",
                moveGroupName = "teleport1"
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
        {
          condition_type = "IsOnGround",
          on_ground = true
        }
      },
      skill_cd = 30,
      skill_desc = "新大招",
      skill_id = 1006,
      skill_rate = 0.5,
      timeline_config = {
        {
          duration_second = 4.7,
          tag_config = {
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "role_skill02",
                moveGroupName = "skill02_run"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddBuff",
                buffId = 10018,
                buffLayer = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddEffect",
                addActorConfig = {
                  actorType = "self"
                },
                addParentType = "actor",
                addPositionConfig = {
                  getType = "getBaseOnWorld",
                  worldAbsolutePosition = {
                    0,
                    0,
                    0
                  }
                },
                addRotationConfig = {
                  getType = "getBaseOnWorld",
                  worldAbsoluteRotation = {
                    0,
                    0,
                    0
                  }
                },
                effectId = 1001001,
                scale = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 1,
                  isActivate = false
                },
                isEndEnable = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 1,
                  isActivate = true
                },
                isEndEnable = 1
              },
              trigger_second = 4.7
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 100,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    2,
                    1
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001012,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                },
                impulseId = 104
              },
              trigger_second = 0.3
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 100,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    2,
                    1
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001012,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                },
                impulseId = 104
              },
              trigger_second = 1
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 100,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    2,
                    1
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001012,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                },
                impulseId = 104
              },
              trigger_second = 1.5
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 200,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    2,
                    1
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001012,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                },
                impulseId = 104
              },
              trigger_second = 2
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 200,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    2,
                    1
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001012,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                },
                impulseId = 104
              },
              trigger_second = 2.5
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 200,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    2,
                    1
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001012,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                },
                impulseId = 104
              },
              trigger_second = 3.1
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 200,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    2,
                    2,
                    1
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001012,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                },
                impulseId = 104
              },
              trigger_second = 4.4
            },
            {
              action = {
                action_type = "ShakeCam",
                config = {
                  impulseId = 101
                }
              },
              trigger_second = 3.6
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
        {
          condition_type = "IsOnGround",
          on_ground = true
        }
      },
      skill_cd = 5,
      skill_desc = "后退小技能",
      skill_id = 1007,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 1.133,
          tag_config = {
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "role_skill01",
                moveGroupName = "role_skill01"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddEffect",
                addActorConfig = {
                  actorType = "self"
                },
                addParentType = "actor",
                addPositionConfig = {
                  getType = "getBaseOnWorld",
                  worldAbsolutePosition = {
                    0,
                    0,
                    0
                  }
                },
                addRotationConfig = {
                  getType = "getBaseOnWorld",
                  worldAbsoluteRotation = {
                    0,
                    0,
                    0
                  }
                },
                effectId = 1011,
                scale = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      3,
                      0.6,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 100,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    2,
                    1,
                    0.8
                  },
                  soundEvent = "skill_zj_impact"
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001012,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 0.66
            },
            {
              action = {
                action_type = "ShakeCam",
                config = {
                  impulseId = 101
                }
              },
              trigger_second = 0.4
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
        {
          condition_type = "IsOnGround",
          on_ground = true
        }
      },
      skill_cd = 12,
      skill_desc = "旧大招",
      skill_id = 1008,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 1.7,
          tag_config = {
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "role_skill02",
                moveGroupName = "role_skill02"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 1,
                  isActivate = false
                },
                isEndEnable = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      1,
                      0.6,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1.5,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 100,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    1,
                    0.8
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1010,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 0.5
            },
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    1.5,
                    1.3,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 1016,
                scale = 1
              },
              trigger_second = 0.5
            },
            {
              action = {
                action_type = "ShakeCam",
                config = {
                  impulseId = 102
                }
              },
              trigger_second = 0.5
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      1,
                      0.6,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 2,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 100,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    1,
                    0.8
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1010,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 1.2
            },
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    2.5,
                    1.6,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 1017,
                scale = 1
              },
              trigger_second = 1.2
            },
            {
              action = {
                action_type = "ShakeCam",
                config = {
                  impulseId = 103
                }
              },
              trigger_second = 1.2
            },
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 2
              },
              trigger_second = 1.6
            }
          },
          timeline_id = 1
        },
        {
          duration_second = 1.7,
          tag_config = {
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "role_skill02_2",
                moveGroupName = "role_skill02_2"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 1,
                  isActivate = false
                },
                isEndEnable = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      1,
                      0.6,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1.5,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 100,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    1,
                    0.8
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1010,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 0.5
            },
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    1.5,
                    1.3,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 1016,
                scale = 1
              },
              trigger_second = 0.5
            },
            {
              action = {
                action_type = "ShakeCam",
                config = {
                  impulseId = 102
                }
              },
              trigger_second = 0.5
            },
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      1,
                      0.6,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 2,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 100,
                  layerMaskName = {
                    "Monster"
                  },
                  size = {
                    1.5,
                    1,
                    0.8
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1010,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 1.2
            },
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    2.5,
                    1.6,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 1017,
                scale = 1
              },
              trigger_second = 1.2
            },
            {
              action = {
                action_type = "ShakeCam",
                config = {
                  impulseId = 103
                }
              },
              trigger_second = 1.2
            }
          },
          timeline_id = 2
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "被动技-落地清除空中起跳buff",
      skill_id = 1009,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "RemoveBuff",
                buffId = 10011
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "ChangeBodyStateAnimation",
                animationKey = "fall",
                bodyState = 3
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 1,
                  isActivate = true
                }
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 3,
                  isActivate = false
                }
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "被动技-跳跃受击盒变小",
      skill_id = 1010,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 1,
                  isActivate = false
                }
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onAir",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 3,
                  isActivate = true
                }
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onAir",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 1,
                  isActivate = true
                }
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "HandleHitBox",
                handleInfo = {
                  groupId = 3,
                  isActivate = false
                }
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "PlaySound",
                soundEvent = "skill_zj_land"
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onGround",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = -1
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 10,
      skill_desc = "滑索-技能测试场景",
      skill_id = 1011,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 2
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "onEnterSlidingRope",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        },
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "stand",
                moveGroupName = "slidingRope1"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 3
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "moveGroupFinish",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            },
            {
              action = {
                PosEventToUI = "hideBattleUI",
                action_type = "PosEventToUI",
                tagDesc = "隐藏战斗ui"
              },
              trigger_second = 0
            }
          },
          timeline_id = 2
        },
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "stand",
                tagDesc = "落地动画"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "ChangeCurStagePathId",
                stagePathId = 3,
                tagDesc = "修改当前所在路线"
              },
              trigger_second = 0
            },
            {
              action = {
                PosEventToUI = "showBattleUI",
                action_type = "PosEventToUI",
                tagDesc = "显示战斗ui"
              },
              trigger_second = 0.5
            },
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 1
              },
              trigger_second = 0.6
            }
          },
          timeline_id = 3
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = false,
      skill_cd = 0,
      skill_desc = "玩家蹲下",
      skill_id = 1012,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = 3,
          tag_config = {
            {
              action = {
                action_type = "PlayerAttackState",
                animationGroupName = "sitDown"
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    }
  }
}