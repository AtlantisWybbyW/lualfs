return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedType = "speed",
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            10,
            0,
            0
          }
        }
      },
      desc = "跳跃攻击",
      isLoop = false,
      name = "rush"
    }
  },
  name = "monster_elite_move"
}