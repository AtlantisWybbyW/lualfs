return
{
  groupList = {
    {
      actionList = {
        {
          animationName = "idle",
          endFrame = 30,
          key = "idle",
          loopType = 2,
          startFrame = 0
        }
      },
      default = true,
      desc = "站立",
      name = "stand"
    },
    {
      actionList = {
        {
          animationName = "run",
          endFrame = 18,
          key = "run",
          loopType = 2,
          startFrame = 0
        }
      },
      behavior = {
        {
          effectConfig = {
            addParentType = "world",
            addPositionConfig = {
              actorConfig = {
                actorType = "self"
              },
              actorPositionType = "relativePosWithFace",
              actorRelativePosition = {
                0,
                0,
                0
              },
              getType = "getBaseOnActor"
            },
            addRotationConfig = {
              actorConfig = {
                actorType = "self"
              },
              actorPositionType = "relativePosWithFace",
              actorRelativeRotation = {
                0,
                0,
                0
              },
              getType = "getBaseOnActor"
            },
            effectId = 1001013,
            scale = 1
          },
          name = "PlayEffect",
          startFrame = 2
        },
        {
          name = "PlaySound",
          soundEvent = "skill_zj_foot",
          startFrame = 2
        },
        {
          effectConfig = {
            addParentType = "world",
            addPositionConfig = {
              actorConfig = {
                actorType = "self"
              },
              actorPositionType = "relativePosWithFace",
              actorRelativePosition = {
                0,
                0,
                0
              },
              getType = "getBaseOnActor"
            },
            addRotationConfig = {
              actorConfig = {
                actorType = "self"
              },
              actorPositionType = "relativePosWithFace",
              actorRelativeRotation = {
                0,
                0,
                0
              },
              getType = "getBaseOnActor"
            },
            effectId = 1001013,
            scale = 1
          },
          name = "PlayEffect",
          startFrame = 11
        },
        {
          name = "PlaySound",
          soundEvent = "skill_zj_foot",
          startFrame = 11
        }
      },
      desc = "跑步",
      name = "run"
    },
    {
      actionList = {
        {
          animationName = "jump_rise",
          endFrame = 14,
          key = "jump_rise",
          loopType = 1,
          startFrame = 0
        }
      },
      behavior = {
        {
          name = "PlaySound",
          soundEvent = "skill_zj_jump",
          startFrame = 0
        }
      },
      desc = "起跳",
      name = "jump"
    },
    {
      actionList = {
        {
          animationName = "jump_fall",
          endFrame = 70,
          key = "jump_fall",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "下落",
      name = "fall"
    },
    {
      actionList = {
        {
          animationName = "jump_fall_2",
          endFrame = 22,
          key = "jump_fall_2",
          loopType = 2,
          startFrame = 16
        }
      },
      desc = "2段跳下落",
      name = "fall_2"
    },
    {
      actionList = {
        {
          animationName = "jump_land",
          endFrame = 70,
          key = "jump_land",
          loopType = 1,
          startFrame = 50
        }
      },
      desc = "落地",
      name = "landing"
    },
    {
      actionList = {
        {
          animationName = "die",
          endFrame = 100,
          key = "die",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "死亡到底",
      name = "dead"
    },
    {
      actionList = {
        {
          animationName = "role_skill01",
          endFrame = 34,
          key = "role_skill01",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "小技能1，攻击后退",
      name = "role_skill01"
    },
    {
      actionList = {
        {
          animationName = "attack",
          endFrame = 15,
          key = "attack",
          loopType = 1,
          startFrame = 0
        }
      },
      behavior = {
        {
          name = "PlaySound",
          soundEvent = "skill_zj_attack",
          startFrame = 2
        }
      },
      desc = "普攻",
      name = "attack"
    },
    {
      actionList = {
        {
          animationName = "role_skill02",
          endFrame = 120,
          key = "role_skill02",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "大招",
      name = "role_skill02"
    },
    {
      actionList = {
        {
          animationName = "role_skill02",
          endFrame = 60,
          key = "role_skill02",
          loopType = 1,
          startFrame = 10
        }
      },
      desc = "大招",
      name = "role_skill02_2"
    },
    {
      actionList = {
        {
          animationName = "crossStart",
          endFrame = 5,
          key = "crossStart",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "冲刺起始",
      name = "crossStart"
    },
    {
      actionList = {
        {
          animationName = "crossEnd",
          endFrame = 10,
          key = "crossEnd",
          loopType = 1,
          startFrame = 5
        }
      },
      desc = "冲刺结束回站立",
      name = "crossEnd"
    },
    {
      actionList = {
        {
          animationName = "sitDown",
          endFrame = 25,
          key = "sitDown",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "蹲下",
      name = "sitDown"
    },
    {
      actionList = {
        {
          animationName = "sitUp",
          endFrame = 42,
          key = "sitUp",
          loopType = 1,
          startFrame = 28
        }
      },
      desc = "蹲下起来",
      name = "sitUp"
    }
  },
  name = "player_animation",
  upperGroupList = {
    {
      actionList = {
        {
          animationName = "empty",
          endFrame = 30,
          key = "empty",
          loopType = 1,
          startFrame = 0
        }
      },
      default = true,
      desc = "无效果",
      name = "empty"
    },
    {
      actionList = {
        {
          animationName = "upperAttack",
          endFrame = 15,
          key = "upperAttack",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "攻击",
      name = "upperAttack"
    }
  }
}