return
{
  name = "thron_skill",
  skillList = {
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_id = 2201,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "AddAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0.3,
                      0.5,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 200,
                  layerMaskName = {
                    "MainCharacter"
                  },
                  size = {
                    1.1,
                    1,
                    1.8
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    }
  }
}