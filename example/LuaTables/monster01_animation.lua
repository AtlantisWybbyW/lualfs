return
{
  groupList = {
    {
      actionList = {
        {
          animationName = "idle",
          endFrame = 210,
          key = "stand",
          loopType = 2,
          startFrame = 180
        }
      },
      default = true,
      desc = "站立",
      name = "stand"
    },
    {
      actionList = {
        {
          animationName = "die",
          endFrame = 210,
          key = "dead",
          loopType = 1,
          startFrame = 180
        }
      },
      desc = "死亡",
      name = "dead"
    },
    {
      actionList = {
        {
          animationName = "walk",
          endFrame = 29,
          key = "walk",
          loopType = 2,
          startFrame = 4
        }
      },
      desc = "走步",
      name = "walk"
    },
    {
      actionList = {
        {
          animationName = "run",
          endFrame = 54,
          key = "run",
          loopType = 2,
          startFrame = 34
        }
      },
      desc = "跑步",
      name = "run"
    },
    {
      actionList = {
        {
          animationName = "skill01_01",
          endFrame = 30,
          key = "skill01_01",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "冲刺前摇",
      name = "skill01_01"
    },
    {
      actionList = {
        {
          animationName = "skill01_02",
          endFrame = 16,
          key = "skill01_02",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "冲刺中",
      name = "skill01_02"
    },
    {
      actionList = {
        {
          animationName = "skill01_03",
          endFrame = 48,
          key = "skill01_03",
          loopType = 2,
          startFrame = 16
        }
      },
      desc = "冲刺结束",
      name = "skill01_03"
    }
  },
  name = "monster01_animation"
}