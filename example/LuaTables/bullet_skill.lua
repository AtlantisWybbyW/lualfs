return
{
  name = "bullet_skill",
  skillList = {
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_id = 2101,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 2
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "hitWall",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        },
        {
          duration_second = 0.1,
          tag_config = {
            {
              action = {
                action_type = "SummonMonster",
                delayTime = 0,
                summonConfig = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      0,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  behaviorTreeName = "stand",
                  effectId = 1004,
                  monsterId = 10004
                }
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "DestroySelf",
                config = {
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 2
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_id = 2102,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 2
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "hitWall",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        },
        {
          duration_second = 0.1,
          tag_config = {
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 1003,
                scale = 1
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "DestroySelf",
                config = {
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 2
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "玩家子弹添加持续攻击盒",
      skill_id = 2103,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "AddContinueAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 0.25,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 10,
                  damageTypeList = {
                    "default"
                  },
                  hitMaxNum = 1,
                  layerMaskName = {
                    "Monster",
                    "Wall"
                  },
                  size = {
                    0.2,
                    0.2,
                    0.2
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001004,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              condition = {
                condition = {
                  actorConfig = {
                    actorType = "parent"
                  },
                  buffId = 10019,
                  condition_type = "HasBuff"
                },
                condition_type = "Not"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddContinueAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 0.25,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 10,
                  damageTypeList = {
                    "default",
                    "alienation"
                  },
                  hitMaxNum = 1,
                  layerMaskName = {
                    "Monster",
                    "Wall"
                  },
                  size = {
                    0.2,
                    0.2,
                    0.2
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1001004,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              condition = {
                actorConfig = {
                  actorType = "parent"
                },
                buffId = 10019,
                condition_type = "HasBuff"
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 1001008,
                scale = 1
              },
              trigger_second = 0.5
            },
            {
              action = {
                action_type = "DestroySelf",
                config = {
                }
              },
              trigger_second = 0.5
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "怪物子弹添加持续攻击盒",
      skill_id = 2104,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "AddContinueAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 10,
                  hitMaxNum = 1,
                  layerMaskName = {
                    "MainCharacter",
                    "Wall"
                  },
                  size = {
                    0.2,
                    0.2,
                    0.2
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 1021,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "球形boss子弹添加持续攻击盒",
      skill_id = 2105,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "AddContinueAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 10,
                  hitMaxNum = 1,
                  layerMaskName = {
                    "MainCharacter",
                    "Wall"
                  },
                  size = {
                    0.2,
                    0.2,
                    0.2
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 2001002,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "球形boss吸怪攻击盒",
      skill_id = 2106,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "AddContinueAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 10,
                  hitMaxNum = 99,
                  layerMaskName = {
                    "MainCharacter"
                  },
                  size = {
                    1,
                    1,
                    0.2
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 2001002,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "蜻蜓落石",
      skill_id = 2107,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "JumpTimeline",
                timelineId = 2
              },
              triggerListener = {
                actorConfig = {
                  actorType = "self"
                },
                eventParam = {
                  eventType = "hitWall",
                  paramType = "NoValue"
                },
                listenerType = "ActorEventAction",
                triggerTimes = 1
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        },
        {
          duration_second = 0,
          tag_config = {
            {
              action = {
                action_type = "AddEffect",
                addParentType = "world",
                addPositionConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativePosition = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                addRotationConfig = {
                  actorConfig = {
                    actorType = "self"
                  },
                  actorPositionType = "relativePosWithFace",
                  actorRelativeRotation = {
                    0,
                    0,
                    0
                  },
                  getType = "getBaseOnActor"
                },
                effectId = 3001004,
                scale = 0.2
              },
              trigger_second = 0
            },
            {
              action = {
                action_type = "DestroySelf",
                config = {
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 2
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "特效子弹添加持续攻击盒",
      skill_id = 2108,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "AddContinueAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 0,
                  hitMaxNum = 1,
                  layerMaskName = {
                    "MainCharacter",
                    "Wall"
                  },
                  size = {
                    0.2,
                    0.2,
                    0.2
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 2001002,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    },
    {
      condition_config = {
      },
      isPassive = true,
      skill_cd = 0,
      skill_desc = "给落石添加持续攻击盒",
      skill_id = 2109,
      skill_rate = 1,
      timeline_config = {
        {
          duration_second = -1,
          tag_config = {
            {
              action = {
                action_type = "AddContinueAttackCollider",
                atkBoxInfo = {
                  addPositionConfig = {
                    actorConfig = {
                      actorType = "self"
                    },
                    actorPositionType = "relativePosWithFace",
                    actorRelativePosition = {
                      0,
                      1,
                      0
                    },
                    getType = "getBaseOnActor"
                  },
                  addRotationConfig = {
                    getType = "getBaseOnWorld",
                    worldAbsoluteRotation = {
                      0,
                      0,
                      0
                    }
                  },
                  atkBoxRate = 1,
                  atkLayer = 1,
                  atkType = 1,
                  damage = 10,
                  hitMaxNum = 1,
                  layerMaskName = {
                    "MainCharacter",
                    "Wall"
                  },
                  size = {
                    0.5,
                    0.5,
                    0.5
                  }
                },
                effectConfig = {
                  {
                    delayTime = 0,
                    effectID = 2001002,
                    isRelative = true,
                    relativePos = {
                      0,
                      0,
                      0
                    },
                    scale = 1
                  }
                }
              },
              trigger_second = 0
            }
          },
          timeline_id = 1
        }
      }
    }
  }
}