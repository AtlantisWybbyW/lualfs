return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 1,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            10,
            0,
            0
          }
        }
      },
      desc = "血池冲刺",
      isLoop = false,
      name = "hideSprint"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地",
          fixedTime = 0.3,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 0.2,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            10,
            0,
            0
          }
        }
      },
      desc = "水平冲刺",
      isLoop = false,
      name = "nearmove"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地",
          fixedTime = 0.3,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 0.2,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            10,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地",
          fixedTime = 0.7,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 0.15,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            10,
            0,
            0
          }
        }
      },
      desc = "水平冲刺2段",
      isLoop = false,
      name = "nearmove_1"
    },
    {
      actions = {
        {
          accelerationY = -20,
          actionType = "constantHWithGravity",
          correctVelocityHType = "positive",
          ignoreY = false,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = -1,
          useInput = false,
          velocity = {
            6,
            10,
            0
          }
        }
      },
      desc = "向前跳跃",
      isLoop = true,
      name = "jumpSprintActor"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地0.3s",
          fixedTime = 0.3,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          accelerationY = -20,
          actionType = "constantHWithGravity",
          correctVelocityHType = "positive",
          ignoreY = false,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = -1,
          useInput = false,
          velocity = {
            12,
            15,
            0
          }
        }
      },
      desc = "向前跳跃",
      isLoop = true,
      name = "jumpSprintActor_2"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地0.3s",
          fixedTime = 0.3,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = -1,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            -30,
            0
          }
        }
      },
      desc = "下落",
      isLoop = true,
      name = "jumpSprintActor_3"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 0.5,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            6,
            0
          }
        },
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 0.5,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            -6,
            0
          }
        }
      },
      desc = "原地上跳",
      isLoop = false,
      name = "jump"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "speedX",
          desc = "向左上角移动",
          fixedSpeed = 20,
          fixedTime = 0.5,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              11,
              4,
              0
            }
          },
          targetPosTriggerType = "groupStart"
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "空中滞空0.1",
          fixedTime = 0.1,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0.01,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "speedX",
          desc = "落地",
          fixedSpeed = 20,
          fixedTime = 0.6,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              9,
              0,
              0
            }
          },
          targetPosTriggerType = "groupStart"
        }
      },
      desc = "向左跳",
      isLoop = false,
      name = "jump_left"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "speedX",
          desc = "向右上角移动",
          fixedSpeed = 20,
          fixedTime = 0.5,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              29,
              4,
              0
            }
          },
          targetPosTriggerType = "groupStart"
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "空中滞空0.1",
          fixedTime = 0.1,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0.01,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "speedX",
          desc = "落地",
          fixedSpeed = 20,
          fixedTime = 0.6,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              30,
              0,
              0
            }
          },
          targetPosTriggerType = "groupStart"
        }
      },
      desc = "向右跳",
      isLoop = false,
      name = "jump_right"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地1s",
          fixedTime = 1,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "teleport",
          changeFaceType = "ignore",
          desc = "瞬移到面前6米",
          moveTargetConfig = {
            correctVelocityHType = "positive",
            relativePosition = {
              4,
              0,
              0
            },
            targetType = "relativePos"
          },
          targetPosTriggerType = "groupStart",
          targetPosition = {
            0,
            0,
            0
          }
        }
      },
      desc = "冲刺跳跃",
      isLoop = false,
      name = "SprintJump_1"
    },
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地1.8s",
          fixedTime = 1.8,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地0.3s",
          fixedTime = 0.3,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        }
      },
      desc = "冲刺跳跃",
      isLoop = false,
      name = "SprintJump_2"
    },
    {
      actions = {
        {
          actionType = "teleport",
          changeFaceType = "ignore",
          desc = "瞬移到目标最近可寻路点",
          moveTargetConfig = {
            targetType = "targetActorNavmesh"
          },
          targetPosTriggerType = "groupStart",
          targetPosition = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地3.6s",
          fixedTime = 3.6,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        }
      },
      desc = "冲刺跳跃",
      isLoop = false,
      name = "SprintJump_3"
    },
    {
      actions = {
        {
          actionType = "teleport",
          changeFaceType = "ignore",
          desc = "瞬移到目标最近可寻路点",
          moveTargetConfig = {
            targetType = "targetActorNavmesh"
          },
          targetPosTriggerType = "groupStart",
          targetPosition = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "原地0.7s",
          fixedTime = 1.4,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "actionStart",
          velocity = {
            0,
            0,
            0
          }
        }
      },
      desc = "冲刺跳跃",
      isLoop = false,
      name = "SprintJump_3_1"
    }
  },
  name = "bossHuman_move"
}