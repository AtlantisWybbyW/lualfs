return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          changeFaceType = "ignore",
          correctVelocityHType = "positive",
          desc = "朝玩家飞行",
          fixedSpeed = 3,
          fixedType = "speed",
          moveTargetConfig = {
            bodyPart = "chest",
            targetType = "targetActor"
          },
          targetPosTriggerType = "actionStart"
        }
      },
      desc = "朝玩家飞行",
      isDefault = true,
      isLoop = false,
      name = "run"
    }
  },
  name = "effectToPlayer_move"
}