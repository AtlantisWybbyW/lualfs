return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 0.5,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            0.01,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 0.7,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            5,
            0,
            0
          }
        }
      },
      desc = "跳跃攻击",
      isLoop = false,
      name = "jumpattack"
    },
    {
      actions = {
        {
          accelerationY = -20,
          actionType = "constantHWithGravity",
          ignoreY = false,
          moveTargetConfig = {
            targetType = "none"
          },
          moveTime = -1,
          useInput = false,
          velocity = {
            0,
            0,
            0
          },
          velocityKey = "jumpTargetVelocity"
        }
      },
      desc = "朝目标跳跃",
      isLoop = false,
      name = "jumpToTarget"
    }
  },
  name = "monster_jump_move"
}