return
{
  groupList = {
    {
      actionList = {
        {
          animationName = "Boss_Ball_idle",
          endFrame = 98,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      default = true,
      desc = "站立",
      name = "stand"
    },
    {
      actionList = {
        {
          animationName = "Boss_Ball_idle",
          endFrame = 85,
          key = "dead",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "死亡",
      name = "dead"
    },
    {
      actionList = {
        {
          animationName = "Boss_Ball_break",
          endFrame = 84,
          key = "stand",
          loopType = 2,
          startFrame = 12
        }
      },
      desc = "挣扎",
      name = "break"
    },
    {
      actionList = {
        {
          animationName = "Boss_Ball_skill1",
          endFrame = 134,
          key = "skill01",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "技能1",
      name = "skill01"
    },
    {
      actionList = {
        {
          animationName = "Boss_Ball_skill2",
          endFrame = 154,
          key = "skill02",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "技能2",
      name = "skill02"
    },
    {
      actionList = {
        {
          animationName = "Boss_Ball_skill3",
          endFrame = 50,
          key = "skill03",
          loopType = 1,
          startFrame = 0
        },
        {
          animationName = "Boss_Ball_idle",
          endFrame = 98,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "技能3",
      name = "skill03"
    },
    {
      actionList = {
        {
          animationName = "Boss_Ball_skill4",
          endFrame = 92,
          key = "skill04",
          loopType = 1,
          startFrame = 0
        },
        {
          animationName = "Boss_Ball_idle",
          endFrame = 98,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "技能4",
      name = "skill04"
    }
  },
  name = "boss_ball_animation"
}