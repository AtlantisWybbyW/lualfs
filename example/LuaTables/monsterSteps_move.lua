return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 1,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            0,
            2.5,
            0
          }
        }
      },
      desc = "上升",
      isLoop = false,
      name = "monsterSteps_up"
    }
  },
  name = "monsterSteps_move"
}