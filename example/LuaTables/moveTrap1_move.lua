return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "上升1m",
          fixedTime = 0.2,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            0,
            5,
            0
          }
        },
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "等待1s",
          fixedTime = 0.67,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            0,
            0,
            0
          }
        },
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "下落1m",
          fixedTime = 0.33,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            0,
            -3,
            0
          }
        },
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "等待1s",
          fixedTime = 0.67,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            0,
            0,
            0
          }
        }
      },
      desc = "上升-等待-下落-等待",
      isDefault = true,
      isLoop = true,
      name = "monsterSteps_up"
    }
  },
  name = "moveTrap1_move"
}