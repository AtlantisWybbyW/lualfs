return
{
  groupList = {
    {
      actionList = {
        {
          animationName = "idle",
          endFrame = 30,
          key = "stand",
          loopType = 2,
          startFrame = 0
        }
      },
      default = true,
      desc = "站立",
      name = "stand"
    },
    {
      actionList = {
        {
          animationName = "die",
          endFrame = 27,
          key = "dead",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "死亡",
      name = "dead"
    },
    {
      actionList = {
        {
          animationName = "run",
          endFrame = 14,
          key = "run",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "跑步",
      name = "run"
    },
    {
      actionList = {
        {
          animationName = "walk",
          endFrame = 32,
          key = "walk",
          loopType = 2,
          startFrame = 0
        }
      },
      desc = "走路",
      name = "walk"
    },
    {
      actionList = {
        {
          animationName = "skill01",
          endFrame = 119,
          key = "skill01",
          loopType = 1,
          startFrame = 0
        }
      },
      desc = "起跳攻击",
      name = "skill01"
    }
  },
  name = "monster_jump_animation"
}