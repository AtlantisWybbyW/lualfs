return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedTime = 0.8,
          fixedType = "time",
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            10,
            0,
            0
          }
        }
      },
      desc = "跳跃攻击",
      isLoop = false,
      name = "rush"
    }
  },
  name = "monster01_move"
}