return
{
  groupList = {
    {
      actions = {
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedSpeed = 2,
          fixedType = "speed",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              -46.06102,
              43.21107,
              69.4911
            }
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            0,
            2.5,
            0
          }
        },
        {
          actionType = "constantVelocity",
          correctVelocityHType = "positive",
          desc = "向当前朝向匀速位移一段时间",
          fixedSpeed = 2,
          fixedType = "speed",
          moveTargetConfig = {
            targetType = "worldPos",
            worldPosition = {
              -51.71102,
              43.21107,
              69.4911
            }
          },
          targetPosTriggerType = "groupStart",
          velocity = {
            0,
            2.5,
            0
          }
        }
      },
      desc = "在几个点来回移动",
      isDefault = true,
      isLoop = true,
      name = "patrol"
    }
  },
  name = "movingSteps3_move"
}