return
{
  groupList = {
    {
      actions = {
        {
          actionType = "fixedTimeChaseTarget",
          desc = "0.5s飞向目标",
          fixedTime = 0.5,
          moveTargetConfig = {
            targetType = "none"
          },
          targetPosTriggerType = "groupStart"
        }
      },
      desc = "向目标移动",
      isDefault = true,
      isLoop = false,
      name = "chaseTarget"
    }
  },
  name = "dieDropItem1_move"
}